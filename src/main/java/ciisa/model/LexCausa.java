/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciisa.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Astaroth
 */
@Entity
@Table(name = "lex_causas")
@NamedQueries({
    @NamedQuery(name = "LexCausa.findAll", query = "SELECT l FROM LexCausa l"),
    @NamedQuery(name = "LexCausa.findByTCodigo", query = "SELECT l FROM LexCausa l WHERE l.tCodigo = :tCodigo"),
    @NamedQuery(name = "LexCausa.findByTFechaIngreso", query = "SELECT l FROM LexCausa l WHERE l.tFechaIngreso = :tFechaIngreso"),
    @NamedQuery(name = "LexCausa.findByTCaratula", query = "SELECT l FROM LexCausa l WHERE l.tCaratula = :tCaratula"),
    @NamedQuery(name = "LexCausa.findByTSegmento", query = "SELECT l FROM LexCausa l WHERE l.tSegmento = :tSegmento"),
    @NamedQuery(name = "LexCausa.findByTEstado", query = "SELECT l FROM LexCausa l WHERE l.tEstado = :tEstado"),
    @NamedQuery(name = "LexCausa.findByTLitigante", query = "SELECT l FROM LexCausa l WHERE l.tLitigante = :tLitigante")})
public class LexCausa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "t_codigo")
    private String tCodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "t_fecha_ingreso")
    private String tFechaIngreso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "t_caratula")
    private String tCaratula;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "t_segmento")
    private String tSegmento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "t_estado")
    private String tEstado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "t_litigante")
    private String tLitigante;

    public LexCausa() {
    }

    public LexCausa(String tCodigo) {
        this.tCodigo = tCodigo;
    }

    public LexCausa(String tCodigo, String tFechaIngreso, String tCaratula, String tSegmento, String tEstado, String tLitigante) {
        this.tCodigo = tCodigo;
        this.tFechaIngreso = tFechaIngreso;
        this.tCaratula = tCaratula;
        this.tSegmento = tSegmento;
        this.tEstado = tEstado;
        this.tLitigante = tLitigante;
    }

    public String getTCodigo() {
        return tCodigo;
    }

    public void setTCodigo(String tCodigo) {
        this.tCodigo = tCodigo;
    }

    public String getTFechaIngreso() {
        return tFechaIngreso;
    }

    public void setTFechaIngreso(String tFechaIngreso) {
        this.tFechaIngreso = tFechaIngreso;
    }

    public String getTCaratula() {
        return tCaratula;
    }

    public void setTCaratula(String tCaratula) {
        this.tCaratula = tCaratula;
    }

    public String getTSegmento() {
        return tSegmento;
    }

    public void setTSegmento(String tSegmento) {
        this.tSegmento = tSegmento;
    }

    public String getTEstado() {
        return tEstado;
    }

    public void setTEstado(String tEstado) {
        this.tEstado = tEstado;
    }

    public String getTLitigante() {
        return tLitigante;
    }

    public void setTLitigante(String tLitigante) {
        this.tLitigante = tLitigante;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tCodigo != null ? tCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LexCausa)) {
            return false;
        }
        LexCausa other = (LexCausa) object;
        if ((this.tCodigo == null && other.tCodigo != null) || (this.tCodigo != null && !this.tCodigo.equals(other.tCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ciisa.model.LexCausa[ tCodigo=" + tCodigo + " ]";
    }
    
}
