/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciisa.model.dao;

import ciisa.model.LexCausa;
import ciisa.model.dao.exceptions.NonexistentEntityException;
import ciisa.model.dao.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Astaroth
 */
public class LexCausaDAO implements Serializable {

    public LexCausaDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(LexCausa lexCausa) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(lexCausa);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findLexCausa(lexCausa.getTCodigo()) != null) {
                throw new PreexistingEntityException("LexCausa " + lexCausa + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(LexCausa lexCausa) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            lexCausa = em.merge(lexCausa);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = lexCausa.getTCodigo();
                if (findLexCausa(id) == null) {
                    throw new NonexistentEntityException("The lexCausa with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            LexCausa lexCausa;
            try {
                lexCausa = em.getReference(LexCausa.class, id);
                lexCausa.getTCodigo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The lexCausa with id " + id + " no longer exists.", enfe);
            }
            em.remove(lexCausa);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<LexCausa> findLexCausaEntities() {
        return findLexCausaEntities(true, -1, -1);
    }

    public List<LexCausa> findLexCausaEntities(int maxResults, int firstResult) {
        return findLexCausaEntities(false, maxResults, firstResult);
    }

    private List<LexCausa> findLexCausaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(LexCausa.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public LexCausa findLexCausa(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(LexCausa.class, id);
        } finally {
            em.close();
        }
    }

    public int getLexCausaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<LexCausa> rt = cq.from(LexCausa.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
