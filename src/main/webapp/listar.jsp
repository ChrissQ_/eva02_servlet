<%-- 
    Document   : listar
    Created on : 27-04-2020, 11:33:59
    Author     : Astaroth
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        
        <div>
            <h1>Litigios</h1>
            <table>
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Fecha Ingreso</th>
                        <th>Carátula</th>
                        <th>Segmento</th>
                        <th>Estado</th>
                        <th>Abogado Litigante</th>
                    </tr>
                </thead>
                <%
                     LexCausaDAO lexDao = new LexCausaDAO();
                     List<LexCausa> lex=lexDao.findLexCausaEntities();
                     Iterator<LexCausa>iter=lex.iterator();
                     LexCausa nLEx=null;
                      while(iter.hasNext()){
                          nLEx=iter.next();
                      

                %>
                <tbody>
                    <tr>
                        <td><%= nLEx.getTCodigo()%></td>
                        <td><%= nLEx.getTFechaIngreso()%></td>
                        <td><%= nLEx.getTCaratula()%></td>
                        <td><%= nLEx.getTSegmento()%></td>
                        <td><%= nLEx.getTEstado()%></td>
                        <td><%= nLEx.getTLitigante()%></td>
                        <td>
                            <a>Editar</a>
                            <a>Eliminar</a>
                        </td>
                    </tr>
                    <%}%>
                </tbody>
            </table>
        </div>
                
    </body>
</html>
